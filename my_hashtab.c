#include "my_hashtab.h"

int hashtab_hash(char *key) {
    int h = 0;
    char *p;
    for(p = key; *p != '\0'; p++) {
        h = h * HASHTAB_MUL + (int)*p;
    }
    return h % HASHTAB_SIZE;
}

void hashtab_init(struct listnode **hashtab) {
    int i;

    for(i = 0; i < HASHTAB_SIZE; i++) {
        hashtab[i] = NULL;
    }
}

void hashtab_add(struct listnode **hashtab, char *key, int type, int level) {
    struct listnode *node;

    int index = hashtab_hash(key);
    node = malloc(sizeof(*node));
    if(node != NULL) {
        node->key = key;
//      node->value = value;
        node->type = type;
        node->level = level;
        node-> next = hashtab[index];
        hashtab[index] = node;
    }
}

void hashtab_delete(struct listnode **hashtab, char *key)
{
    int index;
    struct listnode *p, *prev = NULL;
    index = hashtab_hash(key);
    for (p = hashtab[index]; p != NULL; p = p->next) {
        if (strcmp(p->key, key) == 0) {
            if (prev == NULL)
                hashtab[index] = p->next;
            else
                prev->next = p->next;
            free(p);
            return;
        }
        prev = p;
    }
}

void hashtab_delete_level(struct listnode **hashtab, int level) {
    struct listnode *prev = NULL, *p;

    for(int i = 0; i < HASHTAB_SIZE; i++) {
        prev = NULL;
        for (p = hashtab[i]; p != NULL; p = p->next) {
            if (p->level == level) {
                //printf("DELETE hashtab: level:%d, key: %s\n", p -> level, p -> key);
                if (prev == NULL)
                    hashtab[i] = p->next;
                else
                    prev->next = p->next;
                free(p);
            }
            prev = p;
        }
    }
}

struct listnode *hashtab_lookup(struct listnode **hashtab, char *key) {
    int index;
    struct listnode *node;

    index = hashtab_hash(key);
    for(node = hashtab[index]; node != NULL; node = node->next) {
        if(strcmp(node->key, key) == 0) {
            return node;
        }
    }
    return NULL;
}