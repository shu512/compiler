#include "compiler.h"

int main(int argc, char *argv[]) {
	int reply;
	char filename[100];
	if (argc == 2) {
        strcpy(filename, argv[1]);
    }
    if (access(filename, 0) != 0) {
        printf("Sorry, file not found!\n");
        exit(1);
    }
    
	reply = lexer(filename);
	if (reply) {
		exit(reply);
	}
    parser();
    return 0;
}