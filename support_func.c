#include "compiler.h"

int is_digit(char c) { return ('0' <= c && c <= '9') ? 1 : 0; }

int is_cond(char c, char nextC) { return (((c == '<') || (c == '>') || (c == '=') ) || ( (nextC == '=') && ((c == '!') || (c == '>') || (c == '<')) )  ) ? 1 : 0; }

int is_symbol(char c) { return (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || c == '_' ) ? 1 : 0; }

int is_variable(char* c) {
    if (!is_symbol(c[0])) {
        return 0;
    }
    int len = strlen(c);
    for (int i = 1; i < len; i++) {
        if (!((c[i] >= 'a' && c[i] <= 'z') || (c[i] >= 'A' && c[i] <= 'Z') || c[i] == '_' || c[i] || is_digit(c[i]) ) ) {
            return 0;
        }
    }
    return 1;
}

int is_bkt(char c) { return ((c == '(') || (c == ')') || (c == '{') || (c == '}') || (c == '[') || (c == ']') )  ? 1 : 0; }

int is_sign(char c) { return (c == '+' || c == '-' || c == '/' || c == '*' ) ? 1 : 0; }

int readNextSymbol(FILE * file) { // возвращает следующий символ (не пробел)
    int readSymv;
    do {
        readSymv = fgetc(file);
        if (readSymv == EOF) {
            return EOF;
        }
    } while(readSymv == ' ');
    return readSymv;
}