BIN=compiler
FILES=lexer.c support_func.c parser.c main.c my_hashtab.c
EXAMPLE=Main.java
TMP_FILES=lexer.txt

all: compil run
compil:
	gcc $(FILES) -o $(BIN)
run:
	./$(BIN) $(EXAMPLE)
clear:
	rm $(BIN)
	rm $(TMP_FILES) 