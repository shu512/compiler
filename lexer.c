#include "compiler.h"

#define KEYWORD_COUNT 21

char *keyword[] = { "if", "else", "for", "do", "while", "switch", "case",
                     "void", "char", "int", "float", "double", "long", "String", 
                     "public", "private", "protected", "static",
                     "return", "class", "import", "System"};

int lexer(char filename[100]) {
    FILE *file;
    FILE *lexer = fopen("lexer.txt", "w");
    
    int readSymv;
    int i = 0, line = 1, simv = 0;
    file = fopen(filename, "r");
    if (file == NULL) {
        printf("Sorry, cannot open file!\n");
        return -1;
    }

    fseek(file, 0, SEEK_END);
    long size = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *word = (char *)malloc(size);
    int index_word = 0; // position pointer in word
    char *wordBuf = (char *)malloc(size);
    int crutch = 1;

    while (1) {
        if (crutch) {
            readSymv = fgetc(file);
            simv++;
        } else
            crutch = 1;
        if (readSymv == EOF) {
            if (feof(file) != 0) {
                printf("EOF!\t\t\t\tLocate = <%s:%d:%d>\n", filename, line + 1, simv);
                break;
            } else {
                printf("Error reading file!\n");
                break;
            }
        } else if (readSymv == '\n') {
            line++;
            simv = 0;
            continue;
        } else if (readSymv == ' ') { // this is tab
            continue;
        } else if (readSymv == '[') {
            fprintf(lexer, "[\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == ']') {
            fprintf(lexer, "]\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == '(') {
            fprintf(lexer, "(\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == ')') {
            fprintf(lexer, ")\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == '{') {
            fprintf(lexer, "{\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == '}') {
            fprintf(lexer, "}\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == ';') {
            fprintf(lexer, ";\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == '+') {
            fprintf(lexer, "+\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == '-') {
            fprintf(lexer, "-\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == '*') {
            fprintf(lexer, "*\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == '/') {
            fprintf(lexer, "/\n<%s:%d:%d:>\n", filename, line, simv);
        } else if (readSymv == '=') {
            readSymv = fgetc(file);
            if (readSymv == '=') {
                fprintf(lexer, "==\n<%s:%d:%d:>\n", filename, line, simv);    
            } else {
                fprintf(lexer, "=\n<%s:%d:%d:>\n", filename, line, simv);
                crutch = 0;
                continue;
            }
        } else if (readSymv == '>') {
            readSymv = fgetc(file);
            if (readSymv == '=') {
                fprintf(lexer, ">=\n<%s:%d:%d:>\n", filename, line, simv);    
            } else {
                fprintf(lexer, ">\n<%s:%d:%d:>\n", filename, line, simv);
                crutch = 0;
                continue;
            }
        } else if (readSymv == '!') {
            readSymv = fgetc(file);
            if (readSymv == '=') {
                fprintf(lexer, "!=\n<%s:%d:%d:>\n", filename, line, simv);    
            } else {
                fprintf(lexer, "!\n<%s:%d:%d:>\n", filename, line, simv);
                crutch = 0;
                continue;
            }
        } else if (readSymv == '<') {
            readSymv = fgetc(file);
            if (readSymv == '=') {
                fprintf(lexer, "<=\n<%s:%d:%d:>\n", filename, line, simv);    
            } else {
                fprintf(lexer, "<\n<%s:%d:%d:>\n", filename, line, simv);
                crutch = 0;
                continue;
            }
        } else if (readSymv == '"') {
            do {
                word[index_word] = readSymv;
                ++index_word;
                readSymv = fgetc(file);
                if (readSymv == '"')
                    break;
            } while (readSymv != EOF);
            if (readSymv == EOF) {
                fprintf(stderr, "EOF in string-constant\n");
                exit(1);
            }
            word[index_word++] = readSymv;
            word[index_word] = '\0';
            index_word = 0;
            printf("String-constant   %s%s%s \tLocate = <%s:%d:%d:>\n",PINK, word, RESET, filename,
                line, simv);
            fprintf(lexer, "%s\n<%s:%d:%d:>\n", word, filename, line, simv);
        } else if (('a' <= readSymv && readSymv <= 'z') ||
            ('A' <= readSymv && readSymv <= 'Z') || readSymv == '_') {
                   // Identifier or lexems (Example: for, _ads, a, while)
            index_word = 0;
            do {
                word[index_word] = readSymv;
                ++index_word;
                readSymv = fgetc(file);
            } while (('a' <= readSymv && readSymv <= 'z') ||
                ('A' <= readSymv && readSymv <= 'Z') || readSymv == '_' ||
                is_digit(readSymv));
            word[index_word] = '\0';
            crutch = 0;
            int found = 0;
            for (i = 0; i < KEYWORD_COUNT; i++) {
                if (!(strcmp(word, keyword[i]))) {
                    found = 1;
                    printf("Keyword\t\t  %s%s%s\t\tLocate = <%s:%d:%d:>\n", GREEN, word, RESET, filename,
                        line, simv);
                    fprintf(lexer, "%s\n<%s:%d:%d:>\n", word, filename, line, simv);
                    break;
                }
            }
            if (!found){
                printf("Identifier\t  %s%s%s \t\tLocate = <%s:%d:%d:>\n", DARKBLUE,
                        word, RESET,filename, line, simv);
                fprintf(lexer, "%s\n<%s:%d:%d:>\n", word, filename, line, simv);
            }
            simv += strlen(word);
            index_word = 0;
        } else if (is_digit(readSymv)) {
            char temp[50]; 
            int i = 0;
            while (1) {
                temp[i] = readSymv;
                readSymv = fgetc(file);
                i++;
                simv++;
                if (readSymv == '\n') {
                    line++;
                    simv = 0;
                }
                if (!is_digit(readSymv)) {
                    temp[i] = '\0';
                    break;
                }
            }
            crutch = 0;
            printf("Const\t\t  %sNumber%s \tLocate = <%s:%d:%d:>\n", BLUE, RESET,
            filename, line, simv - i);
            fprintf(lexer, "%s\n<%s:%d:%d:>\n", temp, filename, line, simv);
        }
    }
    if (fclose(file) == EOF) {
        printf("Error close file!\n");
    }
    if (fclose(lexer) == EOF) {
        printf("Error close file lexer!\n");
    }
    printf("------lexer--------------end------\n");
    return 0;
}
