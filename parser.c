#include "compiler.h"

void parser_error(char* error, char* locate) {
    printf("ParserError: '%s%s%s', Locate: %s\n", RED, error, RESET, locate);
}

void parser() {
    FILE* file = fopen("lexer.txt", "r");
    int readSymv;
    int i = 0, line = 0, simv = 0, simvLocate = 0;
    int level = 0;
    struct listnode *hashtab[HASHTAB_SIZE];
    struct listnode *node;

    hashtab_init(hashtab);

    fseek(file, 0, SEEK_END);
    long size = ftell(file);
    fseek(file, 0, SEEK_SET);

    char** tokens = (char **)malloc(sizeof(char*) * size);
    for(int i = 0; i < size; i++) {
        tokens[i] = (char*)malloc(MAX_LEN_TOKEN);
        tokens[i][0] = '\0';
    }
    char** tokensLocate = (char **)malloc(sizeof(char*) * size);
    for(int i = 0; i < size; i++) {
        tokensLocate[i] = (char*)malloc(MAX_LEN_TOKEN);
        tokensLocate[i][0] = '\0';
    }
    while(1) {
        readSymv = fgetc(file);
        if (readSymv == EOF) {
            tokens[line][simv] = '\0';
            tokensLocate[line][simvLocate] = '\0';
            if (feof(file) != 0) {
                printf("Parser: count tokens = %d\n", line);
                break;
            } else {
                printf("Parser: %sERROR%s: reading file!\n", RED, RESET);
                break;
            }
        } else if (readSymv == '\n') {
            tokens[line][simv] = '\0';
            while(1) {
                readSymv = fgetc(file);
                tokensLocate[line][simvLocate] = readSymv;
                simvLocate++;
                if (readSymv == '\n') {
                    tokensLocate[line][simvLocate] = '\0';
                    simvLocate = 0;
                    break;
                }
            }
            line++;
            simv = 0;
            continue;
        } else {
            tokens[line][simv] = readSymv;
            simv++;

        }
    }

    while(i < line){
        if (!strcmp(tokens[i], "{") ) {
            level++;
        }
      
        if (!strcmp(tokens[i], "}") ) {
            hashtab_delete_level(hashtab, level);
            level--;
        }
        if ((!strcmp(tokens[i], "int") ) || (!strcmp(tokens[i], "char*") )) {
            i++;
            if(is_variable(tokens[i])){
                if(!hashtab_lookup(hashtab, tokens[i])) {
                    int type;
                    if ((!strcmp(tokens[i - 1], "int") )) {
                        type = TYPE_INT;
                    } else if ((!strcmp(tokens[i - 1], "char*") )) {
                        type = TYPE_STRING;
                    }
                    hashtab_add(hashtab, tokens[i], type, level);
                    node = hashtab_lookup(hashtab, tokens[i]);
                    printf("node: %s, type: %d byte(s), level: %d\n", node->key, node->type, node -> level);
                } else {
                    node = hashtab_lookup(hashtab, tokens[i]);
                    if (node -> level == level) {
                        parser_error(tokens[i], tokensLocate[i]);
                    } else {
                        int type;
                        if ((!strcmp(tokens[i - 1], "int") )) {
                            type = TYPE_INT;
                        } else if ((!strcmp(tokens[i - 1], "char*") )) {
                            type = TYPE_STRING;
                        }
                        hashtab_add(hashtab, tokens[i], type, level);
                        printf("node: %s, type: %d byte(s), level: %d\n", tokens[i], type, level);
                    }
                }
            }
        }

        if (!strcmp(tokens[i], "for")) {
            int ok = 0;
            i++;
            if (!strcmp(tokens[i], "(") ) {
                i++;
                if ((!strcmp(tokens[i], "int") ) || (!strcmp(tokens[i], "char*") )) {
                    i++;
                    if(is_variable(tokens[i])){
                        if(!hashtab_lookup(hashtab, tokens[i])) {
                            int type;
                            if ((!strcmp(tokens[i - 1], "int") )) {
                                type = TYPE_INT;
                            } else if ((!strcmp(tokens[i - 1], "char*") )) {
                                type = TYPE_STRING;
                            }
                            hashtab_add(hashtab, tokens[i], type, level + 1);
                            node = hashtab_lookup(hashtab, tokens[i]);
                            printf("node: %s, type: %d byte(s), level: %d\n", node->key, node->type, node -> level);
                        } else {
                            node = hashtab_lookup(hashtab, tokens[i]);
                            if (node -> level == level) {
                                parser_error(tokens[i], tokensLocate[i]);
                            } else {
                                int type;
                                if ((!strcmp(tokens[i - 1], "int") )) {
                                    type = TYPE_INT;
                                } else if ((!strcmp(tokens[i - 1], "char*") )) {
                                    type = TYPE_STRING;
                                }
                                hashtab_add(hashtab, tokens[i], type, level + 1);
                                printf("node: %s, type: %d byte(s), level: %d\n", tokens[i], type, level);
                            }
                        }
                    }
                }
                if(is_variable(tokens[i])){
                    i++;
                    if (!strcmp(tokens[i], "=")) {
                        i++;
                        if (is_digit(tokens[i][0])){
                            i++;
                            if (strcmp(tokens[i], " ; ") ) {
                                i++;
                                if(is_variable(tokens[i])){
                                    i++;
                                    if(is_cond(tokens[i][0], tokens[i][1])){
                                        i++;
                                        if(is_variable(tokens[i])|| is_digit(tokens[i][0])){
                                            i++;
                                            if(strcmp(tokens[i], " ; ")){
                                                i++;
                                                if(is_variable(tokens[i])){
                                                    i++;
                                                    if(!strcmp(tokens[i], "=")){
                                                        i++;
                                                        if(is_variable(tokens[i])) {
                                                            i++;
                                                            if(is_sign(tokens[i][0])){
                                                                i++;
                                                                if(is_digit(tokens[i][0])){
                                                                    i++;
                                                                    if (!strcmp(tokens[i], ")") ) {
                                                                        ok = 1;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (ok != 1) {
                parser_error(tokens[i], tokensLocate[i]);
            }
        }

        if (!strcmp(tokens[i], "while")) {
            int ok = 0;
            i++;
            if (!strcmp(tokens[i], "(")) {
                i++;
                if(is_variable(tokens[i]) || is_digit(tokens[i][0])){
                    i++;
                    if ( is_cond(tokens[i][0], tokens[i][1]) ){
                        i++;
                        if(is_variable(tokens[i])|| is_digit(tokens[i][0])){
                            i++;
                            if (!strcmp(tokens[i], ")") ) {
                                ok = 1;
                            }
                        }
                    }
                }
            }
            if (ok != 1) {
                parser_error(tokens[i], tokensLocate[i]);
            }
        }

        if (!strcmp(tokens[i], "if")){
            int ok = 0;
            i++;
            if(!strcmp(tokens[i], "(")){
                i++;
                if(is_variable(tokens[i])|| is_digit(tokens[i][0])){
                    i++;
                    if(is_cond(tokens[i][0], tokens[i][1])){
                        i++;
                        if(is_variable(tokens[i]) || is_digit(tokens[i][0])){
                            i++;
                            if(!strcmp(tokens[i], ")")){
                                ok = 1;
                            }
                        }
                    }
                }
            }
            if (ok != 1) {
                parser_error(tokens[i], tokensLocate[i]);
            }
        }

        i++;
    }
    /*
    for(int i = 0 ; i < HASHTAB_SIZE; i++) {
        if (hashtab[i] != NULL) {
            printf("%s\n", hashtab[i]->key);
            hashtab_delete(hashtab, hashtab[i]->key);
        }
    }
    */

    for(int i = 0; i < size; i++) {
        free(tokens[i]);
    }
    free(tokens);
    for(int i = 0; i < size; i++) {
        free(tokensLocate[i]);
    }
    free(tokensLocate);
    if (fclose(file) == EOF) {
        printf("Parser: %sERROR%s: Error close file lexer.txt!\n", RED, RESET);
    }
    printf("------parser-------------end------\n");
}