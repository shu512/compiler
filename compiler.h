#ifndef COMPILER_H
#define COMPILER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "my_hashtab.h"

#define RESET "\033[0m"
#define YELLOW "\033[1;33m"
#define BLUE "\033[1;36m"
#define RED "\033[1;31m"
#define PINK "\033[1;35m"
#define GREEN "\033[1;32m"
#define DARKBLUE "\033[1;34m"

#define MAX_LEN_TOKEN 100
#define MAX_LEN_FILENAME 100

int lexer(char filename[100]);

void parser_error(char* error, char* locate);
void parser();

int is_digit(char c);
int is_cond(char c, char nextC);
int is_symbol(char c);
int is_variable(char* c);
int is_bkt(char c);
int is_sign(char c);
int readNextSymbol(FILE * file);

#endif