#ifndef MY_HASHTAB_H
#define MY_HASHTAB_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HASHTAB_SIZE 71
#define HASHTAB_MUL 31
#define TYPE_INT 4
#define TYPE_STRING 9

struct listnode {
    char *key;
    int type;
    int level;

    struct listnode *next;
};

int hashtab_hash(char *key);
void hashtab_init(struct listnode **hashtab);
void hashtab_add(struct listnode **hashtab, char *key, int type, int level);
void hashtab_delete(struct listnode **hashtab, char *key);
void hashtab_delete_level(struct listnode **hashtab, int level);
struct listnode *hashtab_lookup(struct listnode **hashtab, char *key);

#endif